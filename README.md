## Objetivo

Este projeto deve ser capaz de receber informações de uma API contendo dois tipos de eventos e apresentar os dados conforme a tela sugerida.


## Motivação

A aplicação deve consumir os dados e armazenar os eventos preferidos pelo usuário.
 

## Arquitetura

A arquitetura escolhida foi a MVC, com aplicações de conceitos de Clean Architecture. Tal solução não costuma ser tão complexa a ponto de exigir uma curva de aprendizagem alta para a manutenção e; nem tão simples de modo a deixar o projeto desorganizado.  

Optou-se por utilizar Xib ao invés de Storyboards pensando que a aplicação deve crescer com o passar do tempo, exigindo um time de desenvolvimento que possa alterar sem muitas dificuldades diferentes partes da app. Cada view importante deve estar em seu próprio arquivo, minimizando problemas de merge no futuro.

As principais dependências são referentes a Camada de Serviços e facilitam escalar a app para futuras e novas APIs.

## Instalação

Para clonar o repositório, basta entrar com o seguinte comando 

```git clone https://bitbucket.org/orafaelreis/ios-recrutment.git --recursive```

Caso o XCode não reconheça os frameworks externos, como o Alamofire, remova-os e adiciona-os novamente em
Project Target > Sympla  > General >  Linked Frameworks and Libraries



* Esse projeto requer um serviço local rodando. Se desejar simular em outras urls favor alterar o arquivo SymplaAPI.swift.


## TODOS

* prover a busca dos eventos
* persistir no iCloud do Usuário
* prover mais telas e mais funcionalidades
* melhorar o cacheamento das imagens.


## Dependências

Optou-se por gerir as depedências via git-submodules ao invés da tradicional ferramenta CocoaPods.  
A opção é devido a dependência que um projeto de app a algumas dificuldades já passadas com essa ferramenta em uso com mais pessoas. Por se tratar de uma solução "mágica" e fácil de usar, vez ou outra algum programador atualiza bibliotecas que não deveriam e comitam mudanças no repositório. Além disso ela altera bastante nosso arquivo de projeto que se algo pára de funcionar o projeto simplesmente não compila mais. 

Este projeto utiliza os seguintes frameworks:

* [Moya](https://github.com/Moya/Moya)

* [Alamofire](https://github.com/Alamofire/Alamofire)

* [ObjectMapper](https://github.com/Hearst-DD/ObjectMapper)

* [Moya-ObjectMapper](https://github.com/bmoliveira/Moya-ObjectMapper)


## Autores

* Rafael Reis - @orafaelreis

## Licença

Ver arquivo de Licença em anexo
