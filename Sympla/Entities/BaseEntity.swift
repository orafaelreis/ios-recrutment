//
//  BaseEntity.swift
//  Sympla
//
//  Created by rafael reis on 31/05/18.
//  Copyright © 2018 Sympla. All rights reserved.
//

import ObjectMapper

public class BaseEntity : Mappable, Equatable, Hashable {
    public var hashValue: Int {
        get {
            return self.id.hashValue
        }
    }
    
    public var id:String!
    
    required public init?(map: Map) {}

    public func mapping(map: Map) {
        id  <- map["id"]
    }
    
    public static func == (lhs: BaseEntity, rhs: BaseEntity) -> Bool {
        return lhs.hashValue == rhs.hashValue
    }
}
