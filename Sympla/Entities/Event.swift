//
//  Event.swift
//  Sympla
//
//  Created by rafael reis on 31/05/18.
//  Copyright © 2018 Sympla. All rights reserved.
//

import ObjectMapper

public class Event: BaseEntity {
    public var name:String! // "Festival Path 2017"
    public var url:String! // "https://test.sympla.com.br/festival-path-2017__115615"
    public var logoUrl:String! // "https://d1gkntzr8mxq7s.cloudfront.net/fe_58b715e0a867e.png"
    public var startDate:Date! // "2017-05-06 07:00:00"
    public var eventType:String! // "NORMAL"
    public var location:Location! //
    
    required public init?(map: Map) {
        super.init(map: map)
    }
    
    public override func mapping(map: Map) {
        super.mapping(map: map)
        
        name <- map["name"]
        url <- map["url"]
        logoUrl <- map["logo_url"]
        startDate <- (map["start_date"], CustomDateFormatTransform(formatString: SymplaAPIConstants.dateFormat))
        eventType <- map["event_type"]
        location <- map["location"]
    }
}
