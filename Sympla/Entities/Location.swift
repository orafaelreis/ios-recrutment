//
//  Location.swift
//  Sympla
//
//  Created by rafael reis on 31/05/18.
//  Copyright © 2018 Sympla. All rights reserved.
//

import ObjectMapper

public class Location: BaseEntity {
    public var name:String! //"Parque das Mangabeiras"
    public var address:String! //"Avenida JosÃ© de PatrocÃ­nio Pontes"
    public var address_num:Int! //"580"
    public var address_alt:String? //""
    public var neighborhood:String! //"Mangabeiras"
    public var city:String! //"Belo Horizonte"
    public var state:String! //"MG"
    public var stateDesc:String! //"Minas Gerais"
    public var zipCode:String! //"30210-090
    
    public override func mapping(map: Map) {
        name  <- map["name"]
        address  <- map["address"]
        address_num  <- map["address_num"]
        address_alt  <- map["address_alt"]
        neighborhood  <- map["neighborhood"]
        city  <- map["city"]
        state  <- map["state"]
        stateDesc  <- map["state_desc"]
        zipCode  <- map["zip_code"]
    }
}
