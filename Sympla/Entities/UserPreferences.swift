//
//  UserPreferences.swift
//  Sympla
//
//  Created by rafael reis on 01/06/18.
//  Copyright © 2018 Sympla. All rights reserved.
//

import Foundation

class UserPreferences {
    var eventType: EventType = .featured
    let userDefaults = UserDefaults.standard
    
    var eventsLikedKey: String {
        return String(describing: self.eventType) + ".EventsLiked.Key"
    }
    
    var eventsLiked: Set<Event> {
        get {
            guard let jsonString = userDefaults.string(forKey: eventsLikedKey) else {
                return []
            }
            return Set<Event>(JSONString: jsonString) ?? []
        } set {
            userDefaults.set(newValue.toJSONString(), forKey: eventsLikedKey)
        }
    }
    
    func clearPreferences() {
        let currValue = self.eventType
        self.eventType = .featured
        self.eventsLiked = []
        self.eventType = .highlighted
        self.eventsLiked = []
        
        self.eventType = currValue//to preserves data
    }
}
