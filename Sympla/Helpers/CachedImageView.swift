//
//  CachedImageView.swift
//  Sympla
//
//  Created by rafael reis on 01/06/18.
//  Copyright © 2018 Sympla. All rights reserved.
//

import UIKit

class CachedImagedView : UIImageView {
    var task: URLSessionDownloadTask!
    lazy var session = URLSession.shared//(configuration: URLSessionConfiguration())
    lazy var cache:NSCache<NSString, AnyObject> = NSCache()
    
    func downloadImage(url: URL) {//}, completion: (CachedResponse?)) {
        if (cache.object(forKey: url.absoluteString as NSString) != nil) {
            self.image = self.cache.object(forKey: url.absoluteString as NSString) as? UIImage
        }
        else {
            self.task = session.downloadTask(with: url, completionHandler: { (location, response, error) -> Void in
                if let data = try? Data(contentsOf: url){
                    DispatchQueue.main.async(execute: { () -> Void in
                        guard let image = UIImage(data: data) else {
                            return
                        }
                        self.image = image
                        self.cache.setObject(image, forKey: (url.absoluteString as NSString))
                    })
                }
            })
            self.task.resume()
        }
    }
}
