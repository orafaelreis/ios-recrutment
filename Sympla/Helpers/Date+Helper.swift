//
//  Date+Helper.swift
//  Sympla
//
//  Created by rafael reis on 01/06/18.
//  Copyright © 2018 Sympla. All rights reserved.
//

import UIKit

extension Date {
    
    struct Gregorian {
        static let calendar = Calendar(identifier: .gregorian)
    }
    
    func isSameDate(_ date:Date) -> Bool {
        let order = Gregorian.calendar.compare(self, to: date, toGranularity: .day)
        switch order {
            
        case .orderedSame:
            return true
        default:
            return false
        }
    }
    var monthMediumName: String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM"
        
        return dateFormatter.string(from: self)
    }
    
    var day: Int {
        return Calendar.current.dateComponents([.day], from: self).day!
    }
    
    var hour: Int {
        return Calendar.current.dateComponents([.hour], from: self).hour!
    }
}
