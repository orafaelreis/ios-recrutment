//
//  LoadingStateView.swift
//  Sympla
//
//  Created by rafael reis on 01/06/18.
//  Copyright © 2018 Sympla. All rights reserved.
//

import UIKit

class LoadingStateView: UIView {
    
    private var activityIndivator: UIActivityIndicatorView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setup()
    }
    
    func start() {
        activityIndivator.startAnimating()
    }
    
    func stop() {
        activityIndivator.stopAnimating()
    }
    
    private func setup() {
        activityIndivator  = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        addSubview(activityIndivator)
        activityIndivator.translatesAutoresizingMaskIntoConstraints = false
        
        activityIndivator.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        activityIndivator.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
    }
}
