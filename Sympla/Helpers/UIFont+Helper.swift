//
//  UIFont+Helper.swift
//  Sympla
//
//  Created by rafael reis on 31/05/18.
//  Copyright © 2018 Sympla. All rights reserved.
//

import UIKit

private var WHATFontName = ""


enum Font {
    case Raleway(FontType), OpenSans(FontType)
    
    func with(size: CGFloat) -> UIFont {
        WHATFontName = familyName
        switch self {
        case .Raleway(let type), .OpenSans(let type):
            return type.with(size: size)
        }
    }
    
    private var familyName: String {
        get {
            switch self {
            case .Raleway:
                return "Raleway"
            case .OpenSans:
                 return "OpenSans"
            }
        }
    }
}

enum FontType: String {
    
    case black = "Black"
    case bold = "Bold"
    case extraBold = "ExtraBold"
    case extraLight = "ExtraLight"
    case light = "Light"
    case medium = "Medium"
    case regular = "Regular"
    case semiBold = "SemiBold"
    case thin = "Thin"
    
    func with(size: CGFloat) -> UIFont {
        if let font = UIFont(name: fullFontName, size: size){
            return font
        }
        fatalError("Font " + fullFontName + " does not exist")
    }
    
    private var fullFontName: String {
        let suffix = ((rawValue.isEmpty) ? "-Regular" : "-" + rawValue)
        return WHATFontName + suffix
    }
}
