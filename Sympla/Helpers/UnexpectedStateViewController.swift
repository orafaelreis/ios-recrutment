//
//  UnexpectedStateViewController.swift
//  Sympla
//
//  Created by rafael reis on 01/06/18.
//  Copyright © 2018 Sympla. All rights reserved.
//

import UIKit


protocol UnexpectedStateViewDelegate {
    func didTouchActionButton()
    func showErrorState(strategy: StateViewStrategy)
}

class UnexpectedStateViewController : UIViewController {
    
    @IBOutlet weak var actionButton: UIButton!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet var imageView: UIImageView!
    
    var delegate:UnexpectedStateViewDelegate?
    var strategy:StateViewStrategy?
    
    @IBAction func actionButton(_ sender: Any) {
        if delegate != nil{
            delegate?.didTouchActionButton()
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView();
    }
    
    
    func setupView(){
        guard let strategy = strategy else{
            return
        }
        
        messageLabel?.text = strategy.title
        actionButton.isHidden = strategy.actionButtonIsHidden
        actionButton?.setTitle(strategy.actionTitle, for: .normal)
        imageView.image = strategy.image
    }
}


protocol StateViewStrategy {
    
    var title: String{ get set }
    var actionTitle: String{ get set }
    var actionButtonIsHidden: Bool{ get set }
    var image: UIImage?{ get set }
}


class EmptyStateViewStrategy : StateViewStrategy {
    
    var title = ""
    var actionTitle = ""
    var actionButtonIsHidden = true
    var image: UIImage? = nil
    
    init(title: String) {
        self.title = title
    }
    
    init(localizedTitle: String, image: UIImage) {
        self.title = NSLocalizedString(localizedTitle, comment: "")
        self.image = image
    }
}

class NetworkIssuesStateViewStrategy : StateViewStrategy{
    var title = NSLocalizedString("unexpected.state.networking.issues", comment: "")
    var actionTitle = NSLocalizedString("unexpected.state.action.tryagain", comment: "")
    var actionButtonIsHidden = false
    var image:UIImage? = UIImage(named:"icErro")
}
