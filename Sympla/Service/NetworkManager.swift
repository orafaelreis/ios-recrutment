//
//  NetworkManager.swift
//  Sympla
//
//  Created by rafael reis on 31/05/18.
//  Copyright © 2018 Home. All rights reserved.
//

import Moya
import ObjectMapper
import Alamofire

protocol Networkable {
    var provider: MoyaProvider<SymplaAPI> { get }
    
    static var hasConnection: Bool { get }
    
    //func getEvents(completion: @escaping SymplaAPIListCompletion<Event>)
}

struct NetworkManager: Networkable {
    
    public var provider: MoyaProvider<SymplaAPI>
    public static var environment: SymplaAPIEnvironment = .development
    static var hasConnection: Bool {
        return Alamofire.NetworkReachabilityManager()?.isReachable ?? false
    }
    
    public init(debugMode:Bool) {
        provider = MoyaProvider<SymplaAPI>(plugins: [NetworkLoggerPlugin.init(verbose: debugMode)])
    }
    
    func getEvents(type:EventType, completion: @escaping (ResponseResult<Event>) -> ()) {
        request(target: .events(type), completion: completion)
    }

    private func request<T: Mappable>(target: SymplaAPI, completion: @escaping (ResponseResult<T>) -> ()) {
        guard NetworkManager.hasConnection else {
            completion(ResponseResult.error(.notConnected))
            return
        }
        
        provider.request(target) { result in
            switch result {

            case let .success(response):
                if (200..<400 ~= (response.statusCode)){
                    do {
                        let mapped = try response.mapObject(SymplaAPIData<T>.self)
                        completion(ResponseResult.success(mapped.data))
                    } catch {
                        completion(ResponseResult.error(.network(error.localizedDescription)))
                    }
                }
                else{
                    //TODO: Handle API message errors
                    completion(ResponseResult.error(.network("Invalid Response")))
                }

            case .failure(_):
                completion(ResponseResult.error(.networkIssues))
            }
        }
    }
}


