//
//  SymplaAPIErrorHandlerProtocol.swift
//  Sympla
//
//  Created by rafael reis on 31/05/18.
//  Copyright © 2018 Sympla. All rights reserved.
//

import Moya

public enum ServiceError {
    case invalidSession(String)
    case network(String)
    case notConnected
    case networkIssues
    case unknow
    
    var message: String {
        switch self {
        case .invalidSession(let m): return m
        case .network(let m): return m
        default: return NSLocalizedString("sympla.service.error." + String(describing: self).lowercased(), comment: "")
        }
    }
}
