//
//  SymplaAPIConstants.swift
//  Sympla
//
//  Created by rafael reis on 31/05/18.
//  Copyright © 2018 Sympla. All rights reserved.
//
import Foundation

import Moya
import ObjectMapper

typealias SymplaAPIListCompletion<T :Mappable> = (ResponseResult<[T]>) -> ()

public enum SymplaAPIEnvironment {
    case production, development, test
}

public enum ResponseResult<T> {
    case success([T])
    case error(ServiceError)
}

struct SymplaAPIConstants {
    static let dateFormat = "yyyy-MM-dd HH:mm:ss"
    static let timeFormat = "HH:mm:ss"
}


struct SymplaAPIData<T : Mappable>: Mappable {
    var data:[T] = []
    
    // MARK: JSON
    init?(map: Map) { }
    
    mutating func mapping(map: Map) {
        data <- map["data"]
    }
}
