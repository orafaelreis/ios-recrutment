//
//  SymplaAPI+Testing.swift
//  Sympla
//
//  Created by rafael reis on 31/05/18.
//  Copyright © 2018 Home. All rights reserved.
//

import Moya

extension SymplaAPI {
    
    var sampleData: Data {
        switch self {
        case let .events(type):
            switch type {
            case .featured: return stubbedResponse("featured-events")
            case .highlighted: return stubbedResponse("highlighted-events")
            }
        }
    }
    
}

func stubbedResponse(_ filename: String) -> Data! {
    @objc class TestClass: NSObject {}
    
    let bundle = Bundle(for: TestClass.self)
    let path = bundle.path(forResource: filename, ofType: "json")
    return try! Data(contentsOf: URL(fileURLWithPath: path!))
}
