//
//  SymplaAPI.swift
//  Sympla
//
//  Created by rafael reis on 31/05/18.
//  Copyright © 2018 Home. All rights reserved.
//

import Foundation
import Moya

enum EventType {
    case featured, highlighted
}

enum SymplaAPI {
    case events(EventType)
}

extension SymplaAPI: TargetType {
    
    var baseURL: URL {
        switch NetworkManager.environment {
        case .production:
            return URL(string: "http://localhost/sympla" )!
//            return URL(string: "https://s3.amazonaws.com/sympla-ios-recruitment/endpoints/featured_events.json")!

        case .development:
            return URL(string: "http://localhost/sympla/dev" )!
//            return URL(string: "https://s3.amazonaws.com/sympla-ios-recruitment/highlighted_events.json")!

        case .test:
            return URL(string: "http://localhost/sympla/test" )!
        }
    }
    
    public var method: Moya.Method {
        switch self {
        default:
            return .get
        }
    }

    var path: String {
        switch self {

        //MARK: Events
        case let .events(type):
            switch type {
                case .featured:
                    return "/featured-events.json"
                case .highlighted:
                    return "/highlighted-events.json"
            }
        }
    }
    
    var task: Task {
        switch self {
        case .events:
            return .requestParameters(parameters: [:], encoding: URLEncoding.default)
        }
    }
    
    var headers: [String : String]? {
        return ["Content-type": "application/json"]
    }
    
}
