//
//  ViewController.swift
//  Sympla
//
//  Created by rafael reis on 30/05/18.
//  Copyright © 2018 Sympla. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController, UnexpectedStateViewDelegate {
    
    var stateViewController:UnexpectedStateViewController!

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK - ErrorStateViewDelegate
    func didTouchActionButton() {
        assertionFailure("this method need to be implemented in subclass")
    }
    
    // MARK - Error State
    func showErrorState(strategy: StateViewStrategy){
        if stateViewController == nil {
            stateViewController = UnexpectedStateViewController(nibName:"StateView", bundle:nil)
            stateViewController.delegate = self
            stateViewController.strategy = strategy
            self.view.addSubview(stateViewController.view)
        }
    }
}

