//
//  FeaturedEventCell.swift
//  Sympla
//
//  Created by rafael reis on 31/05/18.
//  Copyright © 2018 Sympla. All rights reserved.
//

import UIKit

protocol CollectionViewCellDelegate: class{
    func didTouch(in cell:UICollectionViewCell, sender: Any)
}

class EventCell : UICollectionViewCell {
    
    @IBOutlet weak var eventImageView: CachedImagedView!
    @IBOutlet weak var likeButton: UIButton!
    
    @IBOutlet weak var monthLabel: UILabel!
    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var placeLabel: UILabel!
    
    var indexPath:IndexPath?
    weak var delegate:CollectionViewCellDelegate?
    var type:EventType = .featured
    
    override func layoutSubviews() {
        self.layer.cornerRadius = CGFloat(6)
        self.clipsToBounds = true
        super.layoutSubviews()
    }
    
    func updateWithItem(_ event: Event, indexPath: IndexPath) {

        if type == .highlighted {
            titleLabel.text = event.name
            dayLabel.text = String(describing: event.startDate.day)
            timeLabel.text = String(describing: event.startDate.hour) + "h"
            monthLabel.text = event.startDate.monthMediumName
            placeLabel.text = event.location.name
            locationLabel.text = event.location.city + ", "+event.location.stateDesc
        }
        
        eventImageView.image = #imageLiteral(resourceName: "event_placeholder")
        if event.logoUrl != nil {
            eventImageView.downloadImage(url: URL(string: event.logoUrl)!)
        }
        
        self.indexPath = indexPath
        self.bringSubview(toFront: self.likeButton)//there's a unknown View above my button
        likeButton.isSelected = false
        
        let preferences = UserPreferences()
        preferences.eventType = type
        if preferences.eventsLiked.contains(event) {
            self.likeButton.isSelected = true
        }
    }
    
    @IBAction func didTouch(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        self.delegate?.didTouch(in: self, sender: sender)
    }
}
