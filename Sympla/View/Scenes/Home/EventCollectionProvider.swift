//
//  EventCollectionProvider.swift
//  Sympla
//
//  Created by rafael reis on 01/06/18.
//  Copyright © 2018 Sympla. All rights reserved.
//

import UIKit

class EventCollectionProvider: NSObject, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource, UICollectionViewDelegate {
    var events: [Event] = []
    var eventType: EventType = .featured
    
    func collectionView(_ collectionView: UICollectionView,
                        numberOfItemsInSection section: Int) -> Int {
        return events.count
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeue(UICollectionViewCell.self, for: indexPath) as? EventCell
        cell?.updateWithItem(events[indexPath.item], indexPath: indexPath)
        cell?.delegate = self
        return cell!
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        let wsize = collectionView.bounds.size.width

        switch self.eventType {
        case .featured:
            return CGSize(width: wsize * 0.95, height: collectionView.bounds.height)
        case .highlighted:
            return CGSize(width: wsize * 0.70, height: collectionView.bounds.height)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets
    {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
}


extension EventCollectionProvider : CollectionViewCellDelegate {
    func didTouch(in cell: UICollectionViewCell, sender: Any) {
        guard let eventCell = cell as? EventCell,
            eventCell.indexPath != nil else {
                return
                
        }
        let eventSelected = self.events[eventCell.indexPath!.item]
        toolgleBookMarkEvent(type:self.eventType, eventSelected: eventSelected)
    }
    
    func toolgleBookMarkEvent(type:EventType, eventSelected: Event) {
        let preferences = UserPreferences()
        preferences.eventType = type
        var events = preferences.eventsLiked
        events.toogle(value: eventSelected)
        UserPreferences().eventsLiked = events
    }
    
}
