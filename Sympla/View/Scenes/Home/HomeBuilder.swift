//
//  HomeBuilder.swift
//  Sympla
//
//  Created by rafael reis on 01/06/18.
//  Copyright © 2018 Sympla. All rights reserved.
//

import UIKit

class HomeBuilder {
    
    static func instantiate(networkProvider: NetworkManager) -> HomeViewController {
        let nibName = "HomeView"
        let viewController = HomeViewController(nibName: nibName, bundle: nil)
        viewController.networkProvider = networkProvider
        viewController.mainDataSourceProvider = EventCollectionProvider()
        viewController.secondaryDataSourceProvider = EventCollectionProvider()
        return viewController
    }
    
}
