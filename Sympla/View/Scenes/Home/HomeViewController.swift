//
//  HomeViewController.swift
//  Sympla
//
//  Created by rafael reis on 30/05/18.
//  Copyright © 2018 Sympla. All rights reserved.
//

import UIKit

class HomeViewController : BaseViewController {
    
    @IBOutlet weak var logoView: UIImageView!
    @IBOutlet weak var promotionText: UILabel!
    @IBOutlet weak var highlightedLabel: UILabel!
    @IBOutlet weak var mainCollectionView: UICollectionView!
    @IBOutlet weak var secondaryCollectionView: UICollectionView!
    lazy var loadingView = LoadingStateView(frame: self.view.frame)
    var numberOfEvents:Int? {
        didSet {
            configurePromotionTextLabel()
        }
    }
    
    public var networkProvider: NetworkManager!
    public var mainDataSourceProvider: EventCollectionProvider!
    public var secondaryDataSourceProvider: EventCollectionProvider!

    fileprivate func configurePromotionTextLabel() {
        guard let numberOfEvents = numberOfEvents else {
            return
        }
        
        let s1 = String(numberOfEvents) + " eventos. "
        let s2 = "Viva as melhores experiências por todo o Brasil."
        
        let numberOfEventsAttStr = NSAttributedString(string: s1,
                                                      attributes: [.font : Font.Raleway(.bold).with(size: 21),
                                                                   .foregroundColor: UIColor.red,
                                                                   NSAttributedStringKey.kern: 1.5])
        
        let promotionAttStr = NSAttributedString(string: s2,
                                                 attributes: [.font : Font.Raleway(.light).with(size: 21),
                                                              .foregroundColor : UIColor.black,
                                                              NSAttributedStringKey.kern: 1.5])
        
        promotionText.attributedText = numberOfEventsAttStr + promotionAttStr
    }
    
    fileprivate func configureMainCollectionView() {
        guard let collectionView = mainCollectionView else { return }
        
        collectionView.register(EventCell.self)
        collectionView.register(UINib(nibName:"FeatureEventViewCell", bundle: nil), forCellWithReuseIdentifier: EventCell.reuseIdentifier)
        
        collectionView.backgroundColor = collectionView.superview?.backgroundColor
        collectionView.dataSource = mainDataSourceProvider
        collectionView.delegate = mainDataSourceProvider
        mainDataSourceProvider.eventType = .featured
    }
    
    fileprivate func configureSecondaryCollectionView() {
        guard let collectionView = secondaryCollectionView else { return }
        
        collectionView.register(EventCell.self)
        collectionView.register(UINib(nibName:"FeatureEventViewCell", bundle: nil), forCellWithReuseIdentifier: EventCell.reuseIdentifier)
        
        collectionView.backgroundColor = collectionView.superview?.backgroundColor
        collectionView.dataSource = secondaryDataSourceProvider
        collectionView.delegate = secondaryDataSourceProvider
        secondaryDataSourceProvider.eventType = .highlighted
    }
    
    fileprivate func setupView() {
        configurePromotionTextLabel()
        configureMainCollectionView()
        configureSecondaryCollectionView()
        mainCollectionView.reloadData()
        secondaryCollectionView.reloadData()
        
        promotionText.isHidden = false
        logoView.isHidden = false
        highlightedLabel.isHidden = false
    }
    
    fileprivate func preSetupView() {
        promotionText.isHidden = true
        logoView.isHidden = true
        highlightedLabel.isHidden = true
    }
    
    override func viewDidLoad() {
        preSetupView()
        loadData()
    }
    
    override func didTouchActionButton() {
        loadData()
    }
    
    
    func loadData() {
        loadingView.start()
        networkProvider.getEvents(type: .featured) { (eventsData) in
            switch eventsData {
                
            case .success(let featuredEvents) :
                self.networkProvider.getEvents(type: .highlighted) { (eventsData) in
                    switch eventsData {
                        
                    case .success(let highlightedEvents) :
                        self.mainDataSourceProvider.events = featuredEvents
                        self.secondaryDataSourceProvider.events = highlightedEvents
                        self.numberOfEvents = featuredEvents.count + highlightedEvents.count
                        self.setupView()
                    case .error(let error):
                        debugPrint("failed due ", error.message)
                    }
                }
            case .error(let error):
                self.showErrorState(strategy: NetworkIssuesStateViewStrategy())
                debugPrint("failed due ", error.message)
            }
        }
    }
}
