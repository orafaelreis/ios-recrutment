//
//  HomeViewControllerTests.swift
//  Sympla
//
//  Created by rafael reis on 31/05/18.
//  Copyright © 2018 Sympla. All rights reserved.
//

import XCTest
@testable import Sympla

class HomeViewControllerTests: XCTestCase {
    
    var sut: HomeViewController!
    
    override func setUp() {
        super.setUp()
        
        sut = HomeBuilder.instantiate(networkProvider: NetworkManager(debugMode: false))
        
        
        sut.loadViewIfNeeded()
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func test_Views_AfterViewDidLoad_IsNotNil() {
        XCTAssertNotNil(sut.mainCollectionView)
        XCTAssertNotNil(sut.secondaryCollectionView)
        XCTAssertNotNil(sut.promotionText)
    }
    
    func test_CollectionView_hasItems() {
        
    }
    
    func test_LoadingView_SetsCollectionViewDataSource() {
        XCTAssertTrue(sut.mainCollectionView.dataSource is EventCollectionProvider)
    }
    
    func test_LoadingView_SetsCollectionViewDelegate() {
        XCTAssertTrue(sut.mainCollectionView.delegate is EventCollectionProvider)
    }
}
