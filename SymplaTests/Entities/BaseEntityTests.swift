//
//  BaseEntityTests.swift
//  SymplaTests
//
//  Created by rafael reis on 31/05/18.
//  Copyright © 2018 Sympla. All rights reserved.
//

import XCTest

class BaseEntityTests: XCTestCase {
        
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testExample() {
        // Use recording to get started writing UI tests.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func test_Init_WhenGivenId_SetsId() {
        let entity = BaseEntity(JSON: ["id" : "12312"])
        entity?.id = "12312"
        
        XCTAssertEqual(entity?.id, "12312", "should set id")
    }
}
