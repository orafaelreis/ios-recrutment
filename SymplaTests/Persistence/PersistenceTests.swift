//
//  PersistenceTests.swift
//  SymplaTests
//
//  Created by rafael reis on 01/06/18.
//  Copyright © 2018 Sympla. All rights reserved.
//

import XCTest

class UserPreferencePersistenceTests: XCTestCase {
    let sut = UserPreferences()

    
    override func setUp() {
        super.setUp()
        sut.clearPreferences()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func test_LikeEventFromMinasGerais_Persists() {
        let key = sut.featureEventsLikedKey
        
        //store
        let mgEvents = self.getStubbedEvents().filter({ (e) -> Bool in
            return e.location.state.elementsEqual("MG")
        })
        let mgEventsSet = Set<Event>(mgEvents.map { $0 })
        sut.featuredEventsLiked = mgEventsSet
        
        //read by self and assert
        var storedEvents:Set<Event> = []
        
        if let jsonString = UserDefaults.standard.string(forKey: key) {
            storedEvents =  Set<Event>(JSONString: jsonString) ?? []
        }
        
        XCTAssertEqual(storedEvents, mgEventsSet)
    }
    
}
