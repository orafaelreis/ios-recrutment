//
//  EventAPITests.swift
//  SymplaTests
//
//  Created by rafael reis on 31/05/18.
//  Copyright © 2018 Sympla. All rights reserved.
//

import XCTest

class SymplaAPITests: XCTestCase {
    var sut: NetworkManager!

    override func setUp() {
        super.setUp()
        
        sut = NetworkManager(debugMode: true)
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func test_GetFeaturedEvents_Success() {
        let testExpectation = expectation(description:"Fetch events")

        sut.getEvents { (eventsData) in
            switch eventsData {
            
            case .success(let events) :
                XCTAssertEqual(events.count, 7)
                XCTAssertEqual(events.first?.id, "115615")
                XCTAssertEqual(events.first?.name, "Festival Path 2017")
                XCTAssertEqual(events.first?.location.name, "Rua dos Coropés, 88")
                XCTAssertNotNil(events.first?.startDate)
            case .error(let error):
                XCTFail(error.message)
            }
            testExpectation.fulfill()
        }
        
        waitForExpectations(timeout: 30, handler: nil)
    }
    
    func test_GetFeaturedEvents_EqualsStubs() {
        let testExpectation = expectation(description:"Fetch events")
        
        sut.getEvents { (eventsData) in
            switch eventsData {
                
            case .success(let events) :
                XCTAssertEqual(events.first,self.getStubbedEvents().first)
            case .error(let error):
                XCTFail(error.message)
            }
            testExpectation.fulfill()
        }
        
        waitForExpectations(timeout: 30, handler: nil)
    }
    
    
    func test_FeaturedEvents_FailedResponse() {
        NetworkManager.environment = .test
        let testExpectation = expectation(description:"Turns off api response")
        
        sut.getEvents { (eventsData) in
            switch eventsData {
                
            case .success(_) :
                XCTFail("Not expected")
            case .error(let error):
                XCTAssertNotNil(error.message)
            }
            testExpectation.fulfill()
        }
        
        waitForExpectations(timeout: 30, handler: nil)
    }
}
