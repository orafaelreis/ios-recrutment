//
//  XCTest+Helper.swift
//  Sympla
//
//  Created by rafael reis on 01/06/18.
//  Copyright © 2018 Sympla. All rights reserved.
//

import XCTest


extension XCTestCase {
    
    func getStubbedEvents() -> [Event] {
        let eventsData = SymplaAPI.featuredEvents.sampleData
        let eventsDataString = String(data: eventsData, encoding: .utf8)
        let stubEventResult = SymplaAPIData<Event>(JSONString: eventsDataString!)
        
        return stubEventResult?.data ?? []
    }
}
